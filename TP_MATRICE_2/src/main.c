#include<stdlib.h>
#include<stdio.h>
#include<pair.h>



int main(){

    struct matrix *A = matrixInput("data/A.txt");
    struct matrix *B = matrixInput("data/B.txt");
    
    viewMatrix(A, "Matrice A");
    viewMatrix(B, "Matrice B");

    if(A->m == B->m && A->n == B->n){
        struct matrix *C = addMatrix(A, B);
        viewMatrix(C, "Matrice A+B");

        C = multMatrix(A, B);
        viewMatrix(C, "A*B");   
    }

    return 0;
}