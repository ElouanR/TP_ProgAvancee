#include <stdio.h>
#include <stdlib.h>
#include <pair.h>


struct pair * consPair ( int l, int c ){

    struct pair * res = (struct pair *)calloc(1, sizeof(struct pair));
    res->l = l;
    res->c = c;

    return res; 
}


struct pair * cpyPair ( struct pair * P ){

    struct pair * res = (struct pair *)calloc(1, sizeof(struct pair));

    res->l = P->l;
    res->c = P->c;

    return res; 

}


void freePair ( struct pair * P ){

    P->l = 0;
    P->c = 0;
    free(P);
    P = NULL;
}


int pair2ind ( struct pair * p, struct matrix * M ){

    /*
    Soit une matrice de dimension (x,y)
    et un element de coordonnées (a,b)

    Soit V la valeur sur le vecteur, on a :

    V = a*x + b
    
    */

    return p->l * M->n + p->c;

}


struct pair * ind2pair ( int k, struct matrix * M ){
    /*
    Soit V la valeur sur le vecteur
    et une matrice de dimension (x,y)

    On cherche l'élément les dimensions (a,b) en fonction de V on a :
    a = V / x
    b = V % x
    
    */

    struct pair * res = (struct pair *)calloc(1, sizeof(struct pair));

    res->l = k / M->n;
    res->c = k % M->n;

    return res; 


}