#include <matrix.h>
#include <stdio.h>
#include <stdlib.h>
#include <pair.h>


struct matrix * consMatrix ( int n, int m ){

    struct matrix * res = (struct matrix *)calloc(1, sizeof(struct matrix));

    res->values = (double *)calloc(n*m, sizeof(double));

    res->n = n;
    res->m = m;

    return res;
    
}


struct matrix * cpyMatrix ( struct matrix * M ){

    struct matrix * res = (struct matrix *)calloc(1, sizeof(struct matrix));

    int l = M->n;
    int c = M->m;

    for(int i = 0 ; i < l * c ; i++){
        res->values[i] = M->values[i];
    }

    res->n = M->n;
    res->m = M->m;

    return res;

}



void freeMatrix (struct matrix ** M){
    struct matrix * res = (struct matrix *)calloc(1, sizeof(struct matrix));

    int l = (*M)->n;
    int c = (*M)->m;

    for(int i = 0 ; i < l * c ; i++){
        res->values[i] = 0;
    }

    res->n = 0;
    res->m = 0;

    free(*M);

    *M = NULL;
}


void viewMatrix (struct matrix * M, char * entete ){

    printf("\n%s\n", entete);

    int l = M->n;
    int c = M->m;

    for(int i = 0 ; i < l * c ; i++){
        printf("%2.lf |", M->values[i]);


        if((i+1) % M->n == 0){
            printf("\n");
        }
    }



}


struct matrix * scanMatrix (){

    

    int n, m;

    do{
        printf("Saisir le nombre de lignes de la Matrice : "); scanf("%d", &m) ;
    }while(m <= 0);
    do{
        printf("Saisir le nombre de colones de la Matrice : "); scanf("%d", &n) ;
    }while(n <= 0);

    struct matrix * res = consMatrix(n, m);
    
    for(int i = 0 ; i < m*n ; i++){
        printf("\n\tMatrice[%d][%d] = ", i/m, i%m); scanf("%lf", &(res->values[i]));
    }


    return res;
}


struct matrix * addMatrix ( struct matrix * A, struct matrix * B ){

    struct matrix *res = consMatrix(A->n, A->m);

    for(int i = 0 ; i < A->n * A->m; i++){

        res->values[i] = A->values[i] + B->values[i];

    }

    return res;

}


struct matrix * multMatrix ( struct matrix * A, struct matrix * B ){

    struct matrix *res = consMatrix(B->n, B->m);

    for(int i = 0 ; i < B->n ; i++){

        for(int j = 0 ; j < B->m ; j++){

            struct pair *tempPairIJ = consPair(i, j);
            int indIJ = pair2ind(tempPairIJ, res);

            for(int k = 0 ; k < B->n; k++){

                struct pair *tempPairIK = consPair(i, k);
                int indIK = pair2ind(tempPairIK, A);                
                struct pair *tempPairKJ = consPair(k, j);
                int indKJ = pair2ind(tempPairKJ, B);

                res->values[indIJ] += A->values[indIK] * B->values[indKJ];
        
            }
        }
    }

    return res;

}