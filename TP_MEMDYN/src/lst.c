#include "lst.h"
#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions





struct lst_t * new_lst() {
	/**
	 * @note : calloc fonctionne de manière identique à malloc
	 * et de surcroît met à NULL(0) tous les octets alloués
	 **/
	struct lst_t * L = (struct lst_t *)calloc(1,sizeof(struct lst_t));
	assert(L);
	return L;
}


void del_lst(struct lst_t ** ptrL ) {
	free(*ptrL);
	(*ptrL) = NULL;
}


bool empty_lst(const struct lst_t * L ) {
	assert(L);// L doit exister !
	return L->numelm == 0;
}


void cons(struct lst_t * L, int v) {
	

	struct lst_elm_t *E = new_lst_elm(v); 

	//On le met en HEAD de list
	E->suc = L->head;

	L->head = E;

	//On augmente le nbr d element
	L->numelm++;
}


void print_lst(struct lst_t * L ) {
	printf( "[ " );
	for( struct lst_elm_t * E = L->head; E; E = E->suc) {
		printf( "%d ", E->x );
	}
	printf( "]\n\n" );
}




void insert_after(struct lst_t * L, const int value, struct lst_elm_t * place){

	//On insère E DERRIERE Place DONC Place.suc pointe sur E, et E pointe sur l'ancien successeur de place

	if(place == NULL){
		cons(L, value);
	}
	else{

	struct lst_elm_t *E = new_lst_elm(value);
	E->suc = place->suc;
	place->suc = E;

	}

}





void insert_ordered(struct lst_t * L, const int value){

	if(L == NULL || value < getX(L->head)){
		insert_after(L, value, NULL);
	}

	else{
		struct lst_elm_t *E;
		E = L->head;

		while(E->suc != NULL && value > getX(E)){

			E = E->suc;

		}
		
		insert_after(L, value, E);

		
	}
 }