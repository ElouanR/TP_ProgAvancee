#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions
#include "lst_elm.h"

#ifndef _LST_
#define _LST_


struct lst_t {
	struct lst_elm_t * head;
	struct lst_elm_t * tail;
	int numelm;
};


/** @brief Construire une liste vide */
struct lst_t * new_lst();


/** @brief Libèrer la mémoire occupée par la liste */
void del_lst(struct lst_t ** ptrL );


/** @brief Vérifier si la liste L est vide ou pas */
bool empty_lst(const struct lst_t * L);


/** @brief Ajouter en tête de la liste L la valeur v */
void cons(struct lst_t * L, int v);


/** @brief Visualiser les éléments de la liste L */
void print_lst(struct lst_t * L );

/** @brief Insère la veleur Value à la place Place */
void insert_after(struct lst_t * L, const int value, struct lst_elm_t * place);

#endif