#include "point2d.h"

#ifndef _SEGMENT2D_
#define _SEGMENT2D_

struct segment2d {
  struct point2d O, E;
};

struct segment2d newSegment2d();

void viewSegment2d(struct segment2d S);



#endif 