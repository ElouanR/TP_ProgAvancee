#include <stdlib.h>
#include <stdio.h>
#include "segment2d.h"

struct segment2d newSegment2d() {
  struct segment2d S;

  printf("Donnez l'abcisse de l'origine du segment : ");  scanf("%lf", &(S.O.x) );
  printf("Donnez l'ordonnee de l'origine du segment : "); scanf("%lf", &(S.O.y) );

  printf("Donnez l'abcisse de l'extremite du segment : "); scanf("%lf", &(S.E.x));
  printf("Donnez l'ordonnee de l'extremite du segment : "); scanf("%lf", &(S.E.y));

  return S;
}

void viewSegment2d(struct segment2d S) {
  printf("Le segment\n\td'origine : (%lf, %lf)\n\td'extremite (%lf, %lf)\n\n", S.O.x, S.O.y, S.E.x, S.E.y);
}