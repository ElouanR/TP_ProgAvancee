#include <stdlib.h>
#include <stdio.h>
#include "point2d.h"

struct point2d newPoint2d() {
  struct point2d P;
  
  printf("Donnez l'abcisse du point : "); scanf("%lf", &(P.x));
  printf("Donnez l'ordonnee du point : "); scanf("%lf", &(P.y));

  return P;
}


void viewPoint2d(struct point2d P) {
  printf("Le point de coordonnées (%lf,%lf)\n", P.x, P.y);
}