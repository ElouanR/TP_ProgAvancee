/**
	TODO : 
		- Finir fonction del_lst
**/

#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions

#include "listelm.h"
#include "outils.h"
#include "lst.h"



/**----------------------------------------------------------------------------------------**/
/**FONCTIONS DE LISTES**/
/**----------------------------------------------------------------------------------------**/



/** @brief Construire une liste vide */
struct lst_t * new_lst () {
	/**
	 * @note : calloc fonctionne de manière identique à malloc
	 * et de surcroît met à NULL(0) tous les octets alloués
	 **/
	struct lst_t * L = (struct lst_t *) calloc(1, sizeof(struct lst_t) );
	assert(L);
	return L;
}



/** @brief Libèrer la mémoire occupée par la liste */
void del_lst ( struct lst_t ** ptrL , void (* ptrFct ) () ) {
		
	struct lst_elm_t * temp = new_lst_elm(NULL);
	
	for(int i = 0 ; i < (*ptrL)->numelm ; i++){
		temp->suc = (*ptrL)->head->suc;
		

		(* ptrFct )((*ptrL)->head->datum);
		del_lst_elm(&((*ptrL)->head));

		(*ptrL)->head = temp;
	}
	(*ptrL)->head = NULL;
	(*ptrL)->tail = NULL;
	(*ptrL)->numelm = 0;

	free(*ptrL);
}	


/** @brief Vérifier si la liste L est vide ou pas */
bool empty_lst ( const struct lst_t * L){
	return ( L->numelm == 0 ) ;
}


/** @brief Ajouter en tête de la liste L la donnée */
void cons ( struct lst_t * L , void * datum ){
	struct lst_elm_t *E = new_lst_elm( datum ); 

	//On le met en HEAD de list
	E->suc = L->head;

	L->head = E;
	if(L->numelm==0) L->tail = E;
	//On augmente le nbr d element
	L->numelm++;
}


/** @brief Ajouter en queue de la liste L la donnée */
void queue ( struct lst_t * L , void * datum ){
	
	struct lst_elm_t *E = new_lst_elm ( datum );

	L->tail->suc = E ;
	L->tail = E ;
	L->numelm++;
}


/** @brief Consulter la queue de la liste */
struct lst_elm_t * getTail ( struct lst_t * L){
	return L->tail ;
}


/** @brief Consulter le nombre d'éléments rangés dans la liste */
int getNumelm ( struct lst_t * L ){
	return L->numelm ;
}


/** @brief Modifier le nombre d'éléments rangés dans la liste */
void setNumelm ( struct lst_t * L , int numElm ){
	L->numelm = numElm;
}





/**----------------------------------------------------------------------------------------**/
/**POINTEURS DE FONCTION**/
/**----------------------------------------------------------------------------------------**/




/** @brief Visualiser les éléments de la liste L grâce à la fonction pointée par ptrFct **/

void view_lst ( struct lst_t * L , void (* ptrFct )() ){
	for ( struct lst_elm_t * E = L->head ; E ; E = E->suc ) {
		(* ptrFct ) (E->datum);
	}
}



void insert_after(struct lst_t * L, void * datum, struct lst_elm_t * place){

	//On insère E DERRIERE Place DONC Place.suc pointe sur E, et E pointe sur l'ancien successeur de place

	if(place == NULL){
		cons(L, datum);
	}
	else{

		struct lst_elm_t *E = new_lst_elm(datum);
		E->suc = place->suc;
		place->suc = E;
	}

}

/** @brief Insérer dans la liste L la donnée suivant l'ordre donné par la fonction pointée par ptrFct */
void insert_ordered ( struct lst_t * L , void * datum , bool (* ptrFct )() ){

	if( empty_lst(L) || ((*ptrFct)(datum, L->head->datum)) ){
		cons(L, datum);
	}
	
	else if((*ptrFct)(L->tail->datum, datum)) {
		//insert_after(L, datum, L->tail);
		queue(L, datum);
	}
	
	else{
		struct lst_elm_t *E;
		E = L->head;

		while(E->suc != NULL && ( (ptrFct)(E->suc->datum, datum) )){

			E = E->suc;

		}
		
		insert_after(L, datum, E);

		
	}
	

}