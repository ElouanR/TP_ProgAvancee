#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions

#include "outils.h"
#include "listelm.h"
#include "lst.h"


void listehomoreelle() {
	struct lst_t * L = new_lst () ;
	double * v ;

	do {

		double u ;

		printf ( "Entrez un réel (0 pour s'arrêter ) : ");
		scanf("%lf" , &u);
		if ( u == 0 ) break;

		v = ( double *) calloc (1 , sizeof ( double ));
		*v = u;

		insert_ordered ( L , v , &reelcmp);

	} while ( true ) ;

	view_lst(L , &printDouble);
	//del_lst(&L , &rmDouble);
}



void listehomoentiere() {
	struct lst_t * L = new_lst () ;
	int * v ;

	do {

		int u ;

		printf ( "Entrez un entier (0 pour s'arrêter ) : ");
		scanf("%d" , &u);
		if ( u == 0 ) break;

		v = ( int *) calloc (1 , sizeof ( int ));
		*v = u;

		insert_ordered ( L , v , &intcmp);

	} while ( true ) ;

	view_lst(L , &printInteger);
	//del_lst(&L , &rmInteger);
}



/**
	* ALGORITHME (FONCTION PRINCIPALE)
**/
int main() {
	listehomoreelle();
	listehomoentiere();
	return EXIT_SUCCESS;
}