#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions

#include "listelm.h"

/**----------------------------------------------------------------------------------------**/
/**FONCTIONS D'ELEMENTS DE LISTE**/
/**----------------------------------------------------------------------------------------**/



/** @brief create a new list element and store datum into it */
struct lst_elm_t * new_lst_elm ( void * datum ){
	struct lst_elm_t *E = (struct lst_elm_t *) calloc(1,sizeof(struct lst_elm_t));
	E->datum = datum;
	return E;
}


/** @brief destroy the list element pointed by E and set E to NULL */
void del_lst_elm ( struct lst_elm_t ** ptrE ){
	(*ptrE)->datum = NULL;
	(*ptrE)->suc = NULL;
	free(*ptrE);
}


/** @brief retreive the datum of the list element E */
void * getDatum ( struct lst_elm_t * E){
	return E->datum;
}


/** @brief retreive the succesor of the list element E */
struct lst_elm_t * getSuc ( struct lst_elm_t * E ){
	return E->suc;
}

/** @brief modify the date of the list element */
void setDatum ( struct lst_elm_t * E , void * datum ){
	E->datum = datum;
}


/** @brief modify the succesor of the list element */
void setSuc ( struct lst_elm_t * E , struct lst_elm_t * S ){
	E->suc = S;
}
