#include "lst.h"

#ifndef _DB_
#define _DB_

enum mode_t { TEXT, BINARY };

/** write a list into a text or binary file acording to the mode */
void write_list( struct lst_t * L , enum mode_t mode , void (* ptrf ) () );

/** read a list from a text or binary file acording to the mode */
struct lst_t * read_list( enum mode_t mode , void * (* ptrf ) () , void ( ptr_del ) () ) ;

#endif