#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions

#include "lst_elm.h"
#include "lst.h"


/****************
Constructors & co
****************/
struct lst_t * newLst (){
    /**
	 * @note : calloc fonctionne de manière identique à malloc
	 * et de surcroît met à NULL(0) tous les octets alloués
	 **/
	struct lst_t * L = (struct lst_t *) calloc(1, sizeof(struct lst_t) );
	assert(L);
	return L;
}

void freeLst ( struct lst_t * L , void (* ptrf ) () ){

	for ( struct lst_elm_t * E = L->head ; E ; E = E->suc ) {
		L->head->pred = NULL;
		L->head = E->suc;
		E->suc = NULL;

		(* ptrf )(E->data);
	}

	free(L);
	
}

bool emptyLst ( struct lst_t * L ){
    return ( L->numelm == 0 ) ;
}
/********************
Accessors & modifiers
*********************/
struct lst_elm_t * get_head ( struct lst_t * L){
    return L->head;
}

struct lst_elm_t * get_tail ( struct lst_t * L){
    return L->tail;
}

/** Add on head */
void cons ( struct lst_t * L , void * data ){
	struct lst_elm_t *E = new_elmlist( data ); 

	//On le met en HEAD de list
	E->suc = L->head;

    if(L->numelm != 0){
        L->head->pred = E;
	}

	L->head = E;
	if(L->numelm==0) 
        L->tail = E;
	//On augmente le nbr d element
	L->numelm++;
}

/** Add on tail */
void queue ( struct lst_t * L , void * data ){
	
	struct lst_elm_t *E = new_elmlist ( data );

	L->tail->suc = E;
    E->pred = L->tail;
	L->tail = E ;
	L->numelm++;
}

/** Insert data at place pointed by ptrf */
void insert_after(struct lst_t * L, void * datum, struct lst_elm_t * place){

	//On insère E DERRIERE Place DONC Place.suc pointe sur E, et E pointe sur l'ancien successeur de place

	if(place == NULL){
		cons(L, datum);
	}
	else{

		struct lst_elm_t *E = new_elmlist(datum);
		E->suc = place->suc;
		place->suc = E;
	}

}

/** @brief Insérer dans la liste L la donnée suivant l'ordre donné par la fonction pointée par ptrFct */
void insert_ordered ( struct lst_t * L , void * datum , bool (* ptrFct )() ){

	if( emptyLst(L) || ((*ptrFct)(datum, L->head->data)) ){
		cons(L, datum);
	}
	
	else if((*ptrFct)(L->tail->data, datum)) {
		//insert_after(L, datum, L->tail);
		queue(L, datum);
	}
	
	else{
		struct lst_elm_t *E;
		E = L->head;

		while(E->suc != NULL && ( (ptrFct)(E->suc->data, datum) )){

			E = E->suc;

		}
		
		insert_after(L, datum, E);

		
	}
	

}

/** Display list on stdout stream */
void printLst ( struct lst_t * L , void (* ptrf ) () ) {
    for ( struct lst_elm_t * E = L->head ; E ; E = E->suc ) {
		(* ptrf ) (E->data);
	}
}