#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions

#include "db.h"
#include "form.h"
#include "lst_elm.h"
#include "lst.h"


int main () {
    // form read from file function pointer
    struct form * (* ptr_read ) ( FILE * , enum mode_t ) = &read_form ;
    // form view function pointer
    void (* ptr_view ) ( struct form *) = & view_form ;
    // form write into file function pointer
    void (* ptr_write )( struct form *, enum mode_t , FILE *) = &write_form ;
    // form deletion function pointer
    void (* ptr_del )( struct form *) = &del_form ;

    // create form list from text/binary file
    struct lst_t * L = read_list ( TEXT , ( void * (*) () ) ptr_read , ptr_del );
    // view form list
    printLst (L , ptr_view );

    // write form list into text/binary file
    write_list (L , BINARY , ptr_write ) ;
    // delete form list
    freeLst (L , ptr_del);

    // create form list from text/binary file
    struct lst_t * W = read_list ( BINARY , ( void * (*) () ) ptr_read , ptr_del );
    // view form list
    printLst (W , ptr_view );

    // delete form list
    freeLst(W , ptr_del ) ;
}