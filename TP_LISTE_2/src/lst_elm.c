#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions


#include "lst_elm.h"

struct lst_elm_t * new_elmlist ( void * data ){
    struct lst_elm_t *E = (struct lst_elm_t *) calloc(1,sizeof(struct lst_elm_t));
	E->data = data;
	return E;
}

void del_elmlist ( struct lst_elm_t * E , void (* ptrf ) () ){
	//(* ptrf)(E->data);
	//E->suc = E->pred = NULL;
}

struct lst_elm_t * get_suc ( struct lst_elm_t * E ){
    return E->suc;
}

struct lst_elm_t * get_pred ( struct lst_elm_t * E){
    return E->pred;
}

void * get_data ( struct lst_elm_t * E){
    return E->data;
}


void set_suc ( struct lst_elm_t * E , struct lst_elm_t * S ){
    E->suc = S;
}

void set_pred ( struct lst_elm_t * E , struct lst_elm_t * P){
    E->pred = P;
}

void set_data ( struct lst_elm_t * E , void * data ){
    E->data = data;
}

void view_elmlist ( struct lst_elm_t * E , void (* ptrf ) () ){
    (ptrf)(E->data);
}