#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions
#include <string.h>


#include "lst.h"
#include "lst_elm.h"
#include "form.h"
#include "db.h"

void write_list( struct lst_t * L , enum mode_t mode , void (* ecriture ) () ) {
    FILE * fd ;
    char fname [20];

    if ( mode == TEXT ) {
        printf ("\t\tWrite list to text file (. txt )\n\tfile name :");
        scanf ("%s", fname );
        fd = fopen ( fname , "w");
    }
    else {
        printf ("\t\tWrite list to binary file (. bin )\n\tfile name :");
        scanf ("%s", fname );
        fd = fopen ( fname , "wb");
    }
    assert ( fd != NULL );

    rewind(fd);

    for ( struct lst_elm_t * E = L->head ; E ; E = E->suc) {
		(*ecriture)(E->data, mode, fd);
	}

    

    fclose ( fd );
    fd = NULL;
}




struct lst_t * read_list ( enum mode_t mode , void * (* ptr_data ) () , void ( * ptr_del ) () ) {
    /**
    on peut ajouter en argument un pointeur sur la fonction de comparaison des formulaires gt_form si l'on souhaite faire une insertion ordonnée (insert_ordered)
    */
    FILE * fd ;
    char fname [20];



    if ( mode == TEXT ) {
        printf ("\t\tRead list from text file (. txt )\n\tfile name :");
        scanf ("%s", fname );
        fd = fopen ( fname , "r");
    }
    else {
        printf ("\t\tRead list from binary file (. bin )\n\tfile name :");
        scanf ("%s", fname );
        fd = fopen ( fname , "rb");
    }

    assert ( fd != NULL );

    rewind(fd);

    struct lst_t * L = newLst();

    struct form * temp = (* ptr_data )(fd, mode);
    cons(L, temp);

    while( !(feof(fd)) ){
            temp = (* ptr_data )(fd, mode);
            cons(L, temp);
    }

    if(mode == BINARY){
        struct lst_elm_t * E = get_head(L);
        struct lst_elm_t * T = get_suc(E);
        set_pred(T, NULL);
        L->head = T;
        del_elmlist(E, ptr_del);
    }



    fclose ( fd );
    fd = NULL;
    return L;
}