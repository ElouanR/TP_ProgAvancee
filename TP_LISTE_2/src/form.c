/**
 * Erreur fonction write_form ne fonctionne pas

**/


#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions
#include <string.h>


#include "form.h"

void del_form ( struct form * F ){
    
    strcpy(F->product, "");
    F->pbt = 0;
    F->stock = 0;
}


/** Read a form from FILE */
struct form * read_form ( FILE * fd , enum mode_t mode ){

    struct form * formulaire = (struct form *) calloc(1,sizeof(struct form));
    

    if(mode == BINARY){

        fread(formulaire, sizeof(struct form), 1, fd);

    }
    else{

        fscanf(fd, "%s\n%d\n%lf\n", formulaire->product, &formulaire->stock, &formulaire->pbt);

    }

    return formulaire;
}


/** Write a form to FILE */
void write_form ( struct form * F , enum mode_t mode , FILE * fd ){
    if(mode == BINARY){

        fwrite(F, sizeof(struct form) , 1 , fd);

    }
    else{

        fprintf(fd, "%s\n%d\n%lf\n", F->product, F->stock, F->pbt);
        
    }
}


/** Get the product name of the form F */
char * get_product ( struct form * F ){
    return F->product;
}


/** Get the stock of the form F */
int get_stock ( struct form * F ) {
    return F->stock;
}


/** Get the price before tax of the form F */
double get_price ( struct form * F ){
    return F->pbt;
}


/** Display a form on stdout stream */
void view_form ( struct form * F ){
    printf("\t\tProduct name : %s\n", F->product);
    printf("\t\tProduct stock : %d\n", F->stock);
    printf("\t\tProduct price : %lf\n", F->pbt);
}


/** Is F1's product name less than or equal to F2's product name ? */
bool gt_form ( struct form * F1 , struct form * F2 ){
    
    int res =  strcmp(F1->product, F2->product);
    if (res <= 0 )
        return true;
    
    return false;
}